# Gnome-Games Bahasa Melayu (ms)
# Noor Azurah Anuar <gbumla@yahoo.com>, 2002
#
msgid ""
msgstr ""
"Project-Id-Version: Gnome-Games 2.0.x\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-01-31 22:30+0100\n"
"PO-Revision-Date: 2003-11-24 01:54+0800\n"
"Last-Translator: Hasbullah Bin Pit <sebol@ikhlas.com>\n"
"Language-Team: Projek Gabai <gabai-penyumbang@lists.sourceforge.org>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/five-or-more.appdata.xml.in.h:1 ../data/five-or-more.desktop.in.h:1
#: ../data/five-or-more.ui.h:1 ../src/five-or-more.c:1173
#: ../src/five-or-more.c:1724
msgid "Five or More"
msgstr "LImau atau Lebih"

#: ../data/five-or-more.appdata.xml.in.h:2 ../data/five-or-more.desktop.in.h:2
msgid "Remove colored balls from the board by forming lines"
msgstr "Buang bola berwarna drpd papan dengan membentuk barisan"

#: ../data/five-or-more.appdata.xml.in.h:3
msgid ""
"Five or More is the GNOME port of a once-popular computer game. Align five "
"or more objects of the same color into a line to cause them to disappear and "
"score points. Earn many more points by lining up as many objects as possible "
"before clearing them."
msgstr ""

#: ../data/five-or-more.appdata.xml.in.h:4
msgid ""
"More objects appear after every turn. Play for as long as possible, until "
"the board is completely full!"
msgstr ""

#: ../data/five-or-more.appdata.xml.in.h:5
msgid "The GNOME Project"
msgstr ""

#: ../data/five-or-more.desktop.in.h:3
msgid "game;strategy;logic;"
msgstr ""

#: ../data/five-or-more-preferences.ui.h:1 ../data/menu.ui.h:2
#: ../src/five-or-more.c:169
msgid "Preferences"
msgstr "Keutamaan"

#: ../data/five-or-more-preferences.ui.h:2
#, fuzzy
msgid "Appearance"
msgstr "Pen_ampilan"

#: ../data/five-or-more-preferences.ui.h:3
#, fuzzy
msgid "_Image:"
msgstr "Imej tema:"

#: ../data/five-or-more-preferences.ui.h:4
#, fuzzy
msgid "B_ackground color:"
msgstr "Warna latar belakang:"

#: ../data/five-or-more-preferences.ui.h:5
#, fuzzy
msgid "Board Size"
msgstr "Saiz papan"

#: ../data/five-or-more-preferences.ui.h:6
#, fuzzy
msgid "_Small"
msgstr "Kecil"

#: ../data/five-or-more-preferences.ui.h:7
msgid "_Medium"
msgstr "_Medium"

#: ../data/five-or-more-preferences.ui.h:8
#, fuzzy
msgid "_Large"
msgstr "Besar"

#: ../data/five-or-more-preferences.ui.h:9
msgid "General"
msgstr "Umum"

#: ../data/five-or-more-preferences.ui.h:10
#, fuzzy
msgid "_Use fast moves"
msgstr "Guna pergerakan pantas"

#: ../data/five-or-more.ui.h:2
#, fuzzy
msgid "Next:"
msgstr "Bola Berikutnya:"

#: ../data/five-or-more.ui.h:3
msgid "0"
msgstr ""

#: ../data/five-or-more.ui.h:4
msgid "Score:"
msgstr "Markah:"

#: ../data/five-or-more.ui.h:5 ../src/games-scores-dialog.c:384
#, fuzzy
msgid "_New Game"
msgstr "Permainan Baru"

#: ../data/five-or-more.ui.h:6
#, fuzzy
msgid "Start a new puzzle"
msgstr "Mula permainan baru"

#: ../data/menu.ui.h:1
#, fuzzy
msgid "Scores"
msgstr "Markah."

#: ../data/menu.ui.h:3
#, fuzzy
msgid "Help"
msgstr "Bantuan"

#: ../data/menu.ui.h:4
msgid "About"
msgstr ""

#: ../data/menu.ui.h:5
msgid "Quit"
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:1
#, fuzzy
msgid "Playing field size"
msgstr "Bermain di peringkat %s"

#: ../data/org.gnome.five-or-more.gschema.xml.h:2
msgid ""
"Playing field size. 1=Small, 2=Medium, 3=Large. Any other value is invalid."
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:3
msgid "Ball style"
msgstr "Gaya bola"

#: ../data/org.gnome.five-or-more.gschema.xml.h:4
msgid "Ball style. The filename of the images to use for the balls."
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:5
msgid "Background color"
msgstr "Warna latar belakang"

#: ../data/org.gnome.five-or-more.gschema.xml.h:6
msgid "Background color. The hex specification of the background color."
msgstr "Warna latar belakang. Specifikasi heks bagi warna latar belakang."

#: ../data/org.gnome.five-or-more.gschema.xml.h:7
msgid "Time between moves"
msgstr "Masa di antara pergerakan"

#: ../data/org.gnome.five-or-more.gschema.xml.h:8
msgid "Time between moves in milliseconds."
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:9
msgid "Game score"
msgstr "Markah permainan"

#: ../data/org.gnome.five-or-more.gschema.xml.h:10
msgid "Game score from last saved session."
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:11
msgid "Game field"
msgstr "Medan permainan"

#: ../data/org.gnome.five-or-more.gschema.xml.h:12
msgid "Game field from last saved session."
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:13
msgid "Game preview"
msgstr "Prebiu permainan"

#: ../data/org.gnome.five-or-more.gschema.xml.h:14
msgid "Game preview from last saved session."
msgstr ""

#: ../data/org.gnome.five-or-more.gschema.xml.h:15
#, fuzzy
msgid "Width of the window in pixels"
msgstr "Lebar bagi tetingkap utama, dalam piksel, pada permulaan"

#: ../data/org.gnome.five-or-more.gschema.xml.h:16
#, fuzzy
msgid "Height of the window in pixels"
msgstr "Tinggi bagi tetingkap utama, dalam piksel, pada permulaan"

#: ../data/org.gnome.five-or-more.gschema.xml.h:17
msgid "true if the window is maximized"
msgstr ""

#: ../src/five-or-more.c:78
#, fuzzy
msgctxt "board size"
msgid "Small"
msgstr "Kecil"

#: ../src/five-or-more.c:79
#, fuzzy
msgctxt "board size"
msgid "Medium"
msgstr "Medium"

#: ../src/five-or-more.c:80
#, fuzzy
msgctxt "board size"
msgid "Large"
msgstr "Besar"

#: ../src/five-or-more.c:163
#, fuzzy
msgid "Could not load theme"
msgstr "Ralat memuatkan tema"

#: ../src/five-or-more.c:189
#, c-format
msgid ""
"Unable to locate file:\n"
"%s\n"
"\n"
"The default theme will be loaded instead."
msgstr ""

#: ../src/five-or-more.c:196
#, fuzzy, c-format
msgid ""
"Unable to locate file:\n"
"%s\n"
"\n"
"Please check that Five or More is installed correctly."
msgstr ""
"Glines tidak menjumpai fail imej:\n"
"%s\n"
"\n"
"Sila semak pemasangan Glines"

#: ../src/five-or-more.c:408
#, fuzzy
msgid "Match five objects of the same type in a row to score!"
msgstr "Padankan lima bebola dengan warna yang sama untuk mendapatkan markah!"

#: ../src/five-or-more.c:469
#, fuzzy
msgid "Five or More Scores"
msgstr "LImau atau Lebih"

#: ../src/five-or-more.c:471
#, fuzzy
msgid "_Board size:"
msgstr "Saiz papan"

#: ../src/five-or-more.c:488
msgid "Game Over!"
msgstr "Permainan Tamat!"

#. Can't move there!
#: ../src/five-or-more.c:645
#, fuzzy
msgid "You can’t move there!"
msgstr "Tidak boleh gerakkan disitu!"

#: ../src/five-or-more.c:1175
msgid "GNOME port of the once-popular Color Lines game"
msgstr "Port Gnome berdasarkan permainan Color Lines yang pernah popular"

#: ../src/five-or-more.c:1181
msgid "translator-credits"
msgstr ""
"\"kenang daku semasa bermain..ahaks...'\n"
"Noor Azurah Anuar <gbumla@yahoo.com>, Bandar Hilir."

#: ../src/five-or-more.c:1305
#, fuzzy
msgid "Are you sure you want to restart the game?"
msgstr "Telah digunakan! Dimana anda ingin meletakkanya?"

#: ../src/five-or-more.c:1308
msgid "_Cancel"
msgstr ""

#: ../src/five-or-more.c:1309
#, fuzzy
msgid "_Restart"
msgstr "Mula semula"

#. Score dialog column header for the score time (e.g. 1 minute)
#: ../src/games-scores-dialog.c:131
#, fuzzy
msgctxt "score-dialog"
msgid "Time"
msgstr "Masa:"

#. Score dialog column header for the score value (e.g. 30 moves)
#: ../src/games-scores-dialog.c:137
#, fuzzy
msgctxt "score-dialog"
msgid "Score"
msgstr "Markah."

#. Score format for time based scores.  %1$d is the time in minutes, %2$d is the time in seconds
#: ../src/games-scores-dialog.c:224
#, c-format
msgctxt "score-dialog"
msgid "%1$dm %2$ds"
msgstr ""

#: ../src/games-scores-dialog.c:370
msgid "_Quit"
msgstr ""

#: ../src/games-scores-dialog.c:377
#, fuzzy
msgid "_Undo"
msgstr "_Pindah"

#: ../src/games-scores-dialog.c:391
msgid "_Close"
msgstr ""

#. Score dialog column header for the date the score was recorded
#: ../src/games-scores-dialog.c:477
#, fuzzy
msgid "Date"
msgstr "Kilas"
